# Greyhound Stop Location Extractor
Extracts Greyhound stop locations from the official website, and then places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for session functionality
    * Beautiful Soup 4.x (bs4) for scraping
    * JSON module for JSON-based geodata
    * Xml.sax.saxutils for the escape method, used to escape/convert string text to be valid XML/KML 
    * Simplekml for easily building KML files
* Also of course depends on the official [Greyhound](https://bustracker.greyhound.com/stop-finder/) webpage.
