#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import json
from xml.sax.saxutils import escape
import simplekml

#Set filename/path for KML file output
kmlfile = "greyhound.kml"
#Set KML schema name
kmlschemaname = "greyhound"
#Set page URL
pageURL = "https://bustracker.greyhound.com/stop-finder/"
#Start a session with the given page URL
session = requests.Session()
page = session.get(pageURL)
#Soupify the HTML
soup = BeautifulSoup(pageURL, 'html.parser')
#Find the 23rd instance of the <script> tag - that one contains all the geo points
script = soup.select("script:nth-of-type(21)")
#Convert the contents of the <script> tag to a string, remove the first 217 characters and last 599 characters used in the HTML - it's almost JSON here
pointstring = str(script[0])[217:-599]
#Define a delimiter that if replaced, would put this big list one step away from becoming valid JSON
delimeter = ");\r\n            stopArray.push("
#Replace the delimeter with a comma and a newline, and surround the whole thing with [square brackets] to specify it as an array
pointstring = "[\n" + pointstring.replace(delimeter,",\n") + "\n]"
#Now it's valid JSON, so we need to make it into a proper JSON object
jsonpoints = json.loads(pointstring)

#Initialize kml object
kml = simplekml.Kml()
#Add schema, which is required for a custom address field
schema = kml.newschema(name=kmlschemaname)
schema.newsimplefield(name="address",type="string")

#Iterate through JSON for each store
for point in jsonpoints:
    #Get the name of the stop
    stopname = escape(point["shortName"])
    #Get the stop address (which is actually in the "name" field) - these are not the cleanest addresses, but luckily we don't need to geocode them
    stopaddress = escape(point["name"])
    #Assign the coordinates from the geocode
    lat = point["stopLatitude"]
    lng = point["stopLongitude"]
    #First, create the point name and description in the kml
    point = kml.newpoint(name=stopname,description="Greyhound bus stop")
    #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
    point.extendeddata.schemadata.schemaurl = kmlschemaname
    simpledata = point.extendeddata.schemadata.newsimpledata("address",stopname)
    #Finally, add coordinates to the feature
    point.coords=[(lng,lat)]
#Save the final KML file
kml.save(kmlfile)
